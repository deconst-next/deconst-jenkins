# deconst-jenkins
[dev] Working on making deconst work on Jenkins

## Notes

The Jenkinsfile and its related Groovy scripts are all pulled into the
Openshift instance when the master branch is updated. Note that this event will
redeploy Jenkins on the cluster.

The Deconst Jenkins has three different slave node types:
- Python
- Ruby
- NodeJS

These slave nodes are for running different preparers (python and ruby), the
submitter (python), and the asset builder (nodejs). **If you want to add a new
preparer that uses a different language**, you need to go modify the TESLA
deployment chart for Deconst to add a Jenkins slave node with that language
installed. For more information, see the Deconst
[CI/CD](https://github.rackspace.com/tesla/charts/tree/master/app/deconst-cicd)
and
[app](https://github.rackspace.com/tesla/charts/tree/master/app/deconst)
charts.

### Adding a new preparer

To add a new preparer, you need to update the slave nodes if needed as noted
above. Then, you need to add the logic to the `notControlNotPR` and
`notControlPR` methods in the Jenkinsfile to turn on the preparer in the
system.

Note that the simplest way to configure a repo to use a preparer is to add the
`preparer` key to the `_deconst.json` file with the name of your preparer as
defined in your modifications to the Jenkinsfile.

### Adding a new content repo

An important note for content repos: While following the guidelines to add a
content repo through the control repo does work, you must manually trigger the
build on the UI for the first time for the webhook triggering a master build to
fire properly. This is a
[known issue](https://issues.jenkins-ci.org/browse/JENKINS-35132?focusedCommentId=294758&page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel#comment-294758).

## Basic workflow

![workflow](./img/workflow.png)

## Content sequence

![sequence](./img/content.png)

## Control sequence

![sequence](./img/control.png)
